////
/// @page Utility Functions/arrays
///
///
////

export class UtilityFunctionsArraysRecursive {
    /**
     * @description Removes all properties named as propertyName from data recursively
     * @param data - Object's root
     * @param propertyName - Property to be removed
     * @returns
     */
    removePropertyRecursively(data: any, propertyName: string) {
        const treeKeys = Object.keys(data);
        const treeKeysCount = treeKeys?.length;
        for (let index = 0; index < treeKeysCount; index++) {
            const key = treeKeys[index];
            if (key === propertyName) delete data[key];
            if (data[`${key}`] && typeof data[`${key}`] === "object") {
                this.removePropertyRecursively(data[`${key}`], propertyName);
            }
        }
        return data;
    }
}
