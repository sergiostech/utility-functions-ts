
////
/// @page Utility Functions/arrays grouping
///
///
////
import { v4 as uuidv4 } from "uuid";

export class UtilityFunctionsArraysGroups {
  /**
   * @description  Returns data grouped by condition
   * @param {any[]} data
   * @memberOf UtilityFunctionsArraysGrouping
   */
  groupDataArrays(data: any[]) {
    const groups = {} as any[];
    data.forEach(this.setGroupElement.bind(this, groups));
    const dataGrouped = this.getGroupedData(groups);
  }

  private getGroupedData(groups: any[]): any[] {
    return Object.keys(groups).map(this.getGroupElement.bind(this, groups));
  }

  private getGroupElement(groups: any[], group: string): any[] {
    return groups[`${group}`];
  }

  private setGroupElement(groups: any[], element: any): void {
    const propertyName = "values";
    this.setElement(element, groups, propertyName);
  }

  private setElement(data: any, groups: any[], propertyName: string) {
    Object.entries(data).forEach(([key, value]) => {
      groups[`${key}`] = groups[`${key}`] ?? { name: key, values: [] };
      groups[`${key}`][propertyName].push(value);
    });
  }

  /**
   * @description Returns coverageMethod formatted as property name by using regEx expression that removes empty spaces and special characters
   * For undefined coverageMethod it will return a universally unique identifier
   * @private
   * @param  {string} coverageMethod - Name of coverage method
   * @return string
   * @memberof CompliancyMatrixService
   */
  private getAsPropertyName(coverageMethod: string): string {
    let propertyName = uuidv4();
    if (coverageMethod) {
      propertyName = coverageMethod.replace(/[^a-zA-Z0-9]/g, "");
    }
    return propertyName;
  }
}
