
////
/// @page Utility Functions/arrays
///
///
////

export class UtilityFunctionsArrays {

    /**
     * @description Returns true if array has some elements
     *
     * @param {*} data - Array
     * @returns {boolean}
     *
     * @memberOf UtilityFunctionsArrays
     */
    checkIfArrayHasData(data: any): boolean {
        return (data?.length) ? true : false;
    }

    /**
     * @description Returns new array of unique values in which duplicate values have been removed
     *
     * @param {any[]} data - Source string with possible multiple duplicate values
     * @returns {any[]}
     *
     * @memberof UtilityFunctionsArrays
     */
    getUniqueValues(data: any[]): any[] {
        return data.reduce((uniques, element) => uniques.includes(element) ? uniques : [...uniques, element], []);
    }

    /**
     * @description Returns Merged/flattened array of arrays
     *
     * @param {any[]} data - Array of arrays [[1,2], [3,4,5]]
     * @returns {any[]}
     *
     * @memberOf UtilityFunctionsArrays
     */
    mergeAnArrayOfArrays(data: any[]): any[] {
        return [].concat(...data);
    }

    /**
     * @description Returns elements from source array that are not present in target array
     * For [1,2,3] [2,3] it will yield [1]. On the other hand, for [1,2,3] [2,3,5] will return the same thing
     * @param {any[]} sourceArray
     * @param {(string | any[])} targetArray
     * @returns
     *
     * @memberOf UtilityFunctionsArrays
     */
    getDifference(sourceArray: any[], targetArray: string | any[]) {
        return sourceArray.filter(x => !targetArray.includes(x));
    }

    /**
     * @description Returns an array containing all the elements of sourceArray that are not in targetArray and vice-versa
     *
     * @param {any[]} sourceArray
     * @param {any[]} targetArray
     * @returns {any[]}
     *
     * @memberOf UtilityFunctionsArrays
     */
    getSymetricDifference(sourceArray: any[], targetArray: any[]): any[] {
        return sourceArray
            .filter(x => !targetArray.includes(x))
            .concat(targetArray.filter(x => !sourceArray.includes(x)));
    }

    /**
     * @description For [1,2,3] [2,3] it will yield [2,3]. On the other hand, for [1,2,3] [2,3,5] will return the same thing
     *
     * @param {any[]} sourceArray
     * @param {(string | any[])} targetArray
     * @returns {any[]}
     *
     * @memberOf UtilityFunctionsArrays
     */
    getIntersection(sourceArray: any[], targetArray: string | any[]): any[] {
        return sourceArray.filter(x => targetArray.includes(x));
    }

}