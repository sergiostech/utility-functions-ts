/**
 * @description Checks if obj is initialized and has type set as object
 * @param obj
 * @returns
 */
export function isObject(obj: { [x: string]: any }) {
    return obj && typeof obj === "object";
}

/**
 * @description Merges source object to target object recursively. Returns merged object.
 * @export
 * @param  {{ [x: string]: any }} target
 * @param  {{ [x: string]: any }} source
 * @return
 */
export function objectsDeepMerge(
    target: { [x: string]: any },
    source: { [x: string]: any }
) {
    if (!isObject(target) || !isObject(source)) {
        return source;
    }
    const keys = Object.keys(source);
    const keysCount = keys?.length;
    for (let index = 0; index < keysCount; index++) {
        const key = keys[index];
        const targetValue = target[key];
        const sourceValue = source[key];

        if (Array.isArray(targetValue) && Array.isArray(sourceValue)) {
            target[`${key}`] = targetValue.concat(sourceValue);
        } else if (isObject(targetValue) && isObject(sourceValue)) {
            target[`${key}`] = objectsDeepMerge(
                Object.assign({}, targetValue),
                sourceValue
            );
        } else {
            target[key] = sourceValue;
        }
    }
    return target;
}
